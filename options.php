<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
    $themename = wp_get_theme();
    $themename = preg_replace("/W/", "_", strtolower($themename) );

    $optionsframework_settings = get_option( 'optionsframework' );
    $optionsframework_settings['id'] = $themename;
    update_option( 'optionsframework', $optionsframework_settings );
	//return 'options-framework-theme';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	$options = array();

	$options[] = array(
		'name' => __( 'Iconos de redes sociales', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array(
        'name' => __( 'Url de la cuenta de facebook', 'theme-textdomain' ),
        'desc' => __( 'Facebook url', 'theme-textdomain' ),
        'id' => 'facebook_url',
        'type' => 'text'
    );

    $options[] = array(
        'name' => __( 'Url de la cuenta de twitter', 'theme-textdomain' ),
        'desc' => __( 'Twitter url', 'theme-textdomain' ),
        'id' => 'twitter_url',
        'type' => 'text'
    );

    $options[] = array(
        'name' => __( 'Url de la cuenta de instragram', 'theme-textdomain' ),
        'desc' => __( 'Instragram url', 'theme-textdomain' ),
        'id' => 'instagram_url',
        'type' => 'text'
    );

    $options[] = array(
        'name' => __( 'Url de la cuenta de google plus', 'theme-textdomain' ),
        'desc' => __( 'Google plus url', 'theme-textdomain' ),
        'id' => 'google_url',
        'type' => 'text'
    );

    $options[] = array(
        'name' => __( 'Url de la cuenta de pinteres', 'theme-textdomain' ),
        'desc' => __( 'Pinterest url', 'theme-textdomain' ),
        'id' => 'pinteres_url',
        'type' => 'text'
    );


	return $options;
}