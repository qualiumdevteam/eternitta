<?php
get_header()
?>
<div class="blog large-12 columns">
    <div class="row">
        <h2 class="titulo_blog"><?php single_tag_title(); ?></h2>
        <div class="divisor"></div>
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4 articulos">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php $imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
                <li>
                    <a href="<?php echo get_the_permalink(get_the_ID()) ?>">
                        <div class="contenido_articulo">
                            <div class="contenido_foto"><div style="background-image: url('<?php echo $imgdestacada; ?>')" class="img_articulo"></div></div>
                                <p class="titulo_articulo"><?php echo get_the_title(); ?></p>
                                <p class="description_articulo"><?php echo substr(strip_tags(get_the_content()),0,100); ?>...</p>
                                <div class="social">
                                    <?php
                                    if ( function_exists( 'sharing_display' ) ) {
                                        sharing_display( '', true );
                                    }

                                    if ( class_exists( 'Jetpack_Likes' ) ) {
                                        $custom_likes = new Jetpack_Likes;
                                        echo $custom_likes->post_likes( '' );
                                    }
                                    ?>
                            </div>
                        </div>
                    </a>
                </li>
            <?php endwhile; ?>
        </ul>
        <div class="paginav"><?php get_pagination($query); ?></div>
    </div>
</div>

<?php get_footer(); ?>