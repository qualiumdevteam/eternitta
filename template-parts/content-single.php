<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eternitta
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="img_destacada" ><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>"></div>

    <header class="entry-header">
        <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
    </header><!-- .entry-header -->

    <div class="fecha_publicado">
        <?php
        $fecha=get_the_date(); $fechats = strtotime($fecha); $fecha_cortada=explode('-',$fecha);
        switch (date('w', $fechats)){
            case 0: $dia="Domingo"; break;
            case 1: $dia="Lunes"; break;
            case 2: $dia="Martes"; break;
            case 3: $dia="Miercoles"; break;
            case 4: $dia="Jueves"; break;
            case 5: $dia="Viernes"; break;
            case 6: $dia="Sabado"; break;
        }
        switch ($fecha_cortada[1]){
            case '01': $mes="Enero"; break;
            case '02': $mes="Febrero"; break;
            case '03': $mes="Marzo"; break;
            case '04': $mes="Abril"; break;
            case '05': $mes="Mayo"; break;
            case '06': $mes="Junio"; break;
            case '07': $mes="Julio"; break;
            case '08': $mes="Agosto"; break;
            case '09': $mes="Septiembre"; break;
            case '10': $mes="Octubre"; break;
            case '11': $mes="Noviembre"; break;
            case '12': $mes="Diciembre"; break;
        }
        ?>
        <p class="fecha_blog">Publicado el <?php echo $fecha_cortada[1]." de ".$mes." de ".$fecha_cortada[2]; ?></p>
    </div>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php eternitta_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

