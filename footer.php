<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eternitta
 */

$facebookurl = of_get_option('facebook_url', '' );
$twitter = of_get_option('twitter_url', '' );
$instagram = of_get_option('instagram_url', '' );
$google_plus = of_get_option('google_url', '' );
$pinteres_url = of_get_option('pinteres_url', '' );
?>
<div class="overlay_loader">
    <div class="loader"></div>
</div>
<div class="large-12 columns" id="footer">
    <div class="row">
        <div class="small-12 medium-6 large-6 columns privacidad">
            <a href="<?php echo site_url() ?>/aviso-privacidad/"><p>Aviso de privacidad</p></a>
            <a href="<?php echo site_url() ?>/politicas-de-pagos/"><p>Politicas de Pago y Envíos</p></a>
        </div>
        <div class="medium-6 large-6 columns text-center">
            <?php if($pinteres_url){ ?>
                <a target="_blank" href="<?php echo $pinteres_url ?>"><div class="glyph-icon flaticon-socialnetwork348 icons iconcircular"></div></a>
            <?php } ?>
            <?php if($google_plus){ ?>
                <a target="_blank" href="<?php echo $google_plus ?>"><div class="glyph-icon flaticon-google-plus icons iconcircular"></div></a>
            <?php } ?>
            <?php if($instagram){ ?>
                <a target="_blank" href="<?php echo $instagram ?>"><div class="glyph-icon flaticon-instagram19 icons iconcircular"></div></a>
            <?php } ?>
            <?php if($twitter){ ?>
                <a target="_blank" href="<?php echo $twitter ?>"><div class="glyph-icon flaticon-twitter icons iconcircular"></div></a>
            <?php } ?>
            <?php if($facebookurl){ ?>
                <a target="_blank" href="<?php echo $facebookurl ?>"><div class="glyph-icon flaticon-facebook2 icons iconcircular"></div></a>
            <?php } ?>
        </div>
        <div class="small-12 medium-12 large-12 columns text-center">
            <!--START Scripts : this is the script part you can add to the header of your theme-->
            <script type="text/javascript" src="http://eternitta.mx/wp-includes/js/jquery/jquery.js?ver=2.6.19"></script>
            <script type="text/javascript" src="http://eternitta.mx/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-es.js?ver=2.6.19"></script>
            <script type="text/javascript" src="http://eternitta.mx/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.6.19"></script>
            <script type="text/javascript" src="http://eternitta.mx/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>
            <script type="text/javascript">
                /* <![CDATA[ */
                var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://eternitta.mx/wp-admin/admin-ajax.php","loadingTrans":"Cargando..."};
                /* ]]> */
            </script><script type="text/javascript" src="http://eternitta.mx/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>
            <!--END Scripts-->

            <div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html563bebcf10d8d-1" class="wysija-msg ajax"></div>
                <form id="form-wysija-html563bebcf10d8d-1" method="post" action="#wysija" class="widget_wysija html_wysija">
                    <p class="leyenda_nosotros">registrate con nosotros</p>
                    <p style="display: inline; width: 40%" class="wysija-paragraph">
                        <?php if(isset($_REQUEST['lang'])){ ?>
                            <input style="display: inline; width: 40%" type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Correo Electrónico" placeholder="E-mail" value="" />
                        <?php } else{ ?>
                            <input style="display: inline; width: 40%" type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Correo Electrónico" placeholder="Correo Electrónico" value="" />
                        <?php } ?>
                        <span class="abs-req">
                            <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
                        </span>
                    </p>
                    <?php if(isset($_REQUEST['lang'])){ ?>
                        <input class="wysija-submit wysija-submit-field" type="submit" value="Send" />
                    <?php }else{ ?>
                        <input class="wysija-submit wysija-submit-field" type="submit" value="Enviar" />
                    <?php } ?>
                    <input type="hidden" name="form_id" value="1" />
                    <input type="hidden" name="action" value="save" />
                    <input type="hidden" name="controller" value="subscribers" />
                    <input type="hidden" value="1" name="wysija-page" />
                    <input type="hidden" name="wysija[user_list][list_ids]" value="1" />

                </form>
            </div>
        </div>
    </div>
</div>
<div class="overlay_multiusos"></div>
<div class="overlay_multiusos2"></div>


<div class="tipo_moneda">
    <!--<a class="closecurrentmoney" href="javascript:void(0)"><div class="glyph-icon flaticon-delete30 close_formaspago"></div></a>-->
    <p class="titulo_login">Selecione un tipo de moneda</p>
    <select name="monedas" class="monedas">
        <option value="">Seleccione una opción</option>
        <option value="MXN">Peso mexicano</option>
        <option value="USD">Dollar</option>
        <option value="EUR">Euro</option>
    </select>
    <div class="botones">
        <a href="javascript:void(0)"><div class="text-center btnlatermoney">Seleccionar mas tarde</div></a>
        <a href="javascript:void(0)"><div class="text-center btncurrentmoney">Aceptar</div></a>
    </div>
</div>

<div class="tipo_moneda_edit">
    <!--<a class="closecurrentmoney" href="javascript:void(0)"><div class="glyph-icon flaticon-delete30 close_formaspago"></div></a>-->
    <p class="titulo_login">Modificar tipo de moneda</p>
    <select name="monedas" class="monedasupdate">
        <option value="">Seleccione una opción</option>
        <option value="MXN">Peso mexicano</option>
        <option value="USD">Dollar</option>
        <option value="EUR">Euro</option>
    </select>
    <div class="botones">
        <a href="javascript:void(0)"><div class="text-center btnlatermoneyupdatecancel">Cancelar</div></a>
        <a href="javascript:void(0)"><div class="text-center btnlatermoneyupdate">Aceptar</div></a>
    </div>
</div>

<div class="login_distribuidores">
    <a class="close_login" href="javascript:void(0)"><div class="glyph-icon flaticon-delete30 close_formaspago"></div></a>
    <p class="titulo_login">Iniciar sesión</p>
    <input type="text" class="email_login" name="username" placeholder="Correo electrónico">
    <input type="password" class="password" name="password" placeholder="Contraseña">
    <a href="javascript:void(0)"><div class="text-center btnlogin">Entrar</div></a>
</div>

<div class="mensaje">
    <h2>Mensaje</h2>
    <p></p>
    <div class="text-center"><a class="btn_cerrar" href="javascript:void(0);">Aceptar</a></div>
</div>

<div class="mensaje_pedido">
    <h2>Mensaje</h2>
    <p></p>
    <div style="text-align: center">
        <div class="text-center btn_confirm"><a class="btn_cerrar_pedido btn_moreproduct" href="javascript:void(0);">Realizar otro pedido</a></div>
        <div class="text-center btn_confirm"><a class="btn_cerrar_pedido btn_Checkout" href="<?php echo site_url(); ?>/pedido">Checkout</a></div>
    </div>
</div>

<div class="mensaje_confirm">
    <input class="identificador" type="hidden" name="identificador">
    <p></p>
    <div class="text-center btns">
        <div data-action="f" class="btn_confirm"><a class="btn_cancel" href="javascript:void(0);">Cancelar</a></div>
        <div data-action="v" class="btn_confirm"><a class="btn_acept" href="javascript:void(0);">Aceptar</a></div>
    </div>
</div>


<div class="mensaje_confirm_pulsera">
    <input class="identificador" type="hidden" name="identificador">
    <p></p>
    <div class="text-center btns_pulsera">
        <div data-action="f" class="btn_confirm"><a class="btn_cancel2" href="javascript:void(0);">Cancelar</a></div>
        <div data-action="v" class="btn_confirm"><a class="btn_acept2" href="javascript:void(0);">Aceptar</a></div>
    </div>
</div>

<div class="edit_pedido">
    <input class="identificador" type="hidden" name="identificador">
    <input class="new_precio" type="hidden" name="new_precio">
    <h2>Actualizar</h2>
    <div class="contenido_modal"></div>
    <div class="text-center">
        <div style="display: inline;"><a class="btn_cancelar_edit" href="javascript:void(0);">Cancelar</a></div>
        <div style="display: inline;"><a class="btn_actualizar" href="javascript:void(0);">Actualizar</a></div>
    </div>
</div>
<div class="modal_formaspago">
    <a class="close_pago" href="#"><div class="glyph-icon flaticon-delete30 close_formaspago"></div></a>
    <h3>Formas de pago</h3>
    <div class="divisor"></div>
    <div class="logos_pagos">
        <div class="small-12 medium-6 large-6 columns text-center paypal">
            <!--<img src="<?php echo get_template_directory_uri() ?>/img/paypal.jpg">-->
            <form name="_xclick" action="https://www.paypal.com/mx/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="info@eternitta.mx">
                <input type="hidden" class="currency_code" name="currency_code" value="MXN">
                <input type="hidden" name="item_name" value="Pedido eternitta">
                <input type="hidden" class="total_res" name="amount" value="">
                <input class="id_pedido_res" type="hidden" name="item_number" value="">
                <input class="boton_pagos" type="image" src="<?php echo get_template_directory_uri() ?>/img/paypal.png" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
            </form>
            <ul>
                <li>Mastercard</li>
                <li>American Express</li>
                <li>Visa</li>
            </ul>
        </div>
        <div class="small-12 medium-6 large-6 columns text-center compropago">
            <!--<img src="<?php echo get_template_directory_uri() ?>/img/compropago.jpg">-->
            <!--<form action="https://www.compropago.com/comprobante" method="post" target="_blank">
                <input type="hidden" name="public_key" value="pk_test_88e8562c21636a591">
                <input type="hidden" class="total_res" name="product_price" value="">
                    <input type="hidden" name="product_name" value="Pedido Eternita">
                <input class="id_pedido_res" type="hidden" name="product_id" value="">
                <input type="hidden" name="image_url" value="">
                <input type="hidden" name="success_url" value="">
                <input type="hidden" name="failed_url" value="">
                <input type="image" class="btn_pago" src="<?php echo get_template_directory_uri() ?>/img/compropago.png" border="0" name="submit" alt="Pagar con ComproPago">
            </form>-->
            <form action="https://compropago.com/comprobante" method="post">
                <input type="hidden" name="public_key" value="pk_live_423d576791b84759b">
                <input type="hidden" class="total_res" name="product_price" value="">
                <input type="hidden" name="product_name" value="Pedido eternitta">
                <input class="id_pedido_res" type="hidden" name="product_id" value="">
                <input type="hidden" name="customer_name" value="">
                <input type="hidden" name="customer_email" value="">
                <input type="hidden" name="customer_phone" value="">
                <input type="hidden" name="image_url" value="">
                <input type="hidden" name="success_url" value="http://eternitta.mx/gracias/">
                <input type="hidden" name="failed_url" value="http://eternitta.mx/">
                <input class="boton_pagos" type="image" src="<?php echo get_template_directory_uri() ?>/img/compropago.png" border="0" name="submit" alt="Pagar con ComproPago">
            </form>

            <ul>
                <li>En efectivo</li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <!--<p class="leyenda_pago">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>-->
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
