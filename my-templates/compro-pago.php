<?php
/*
* Template Name: Compropago
*/
// Recibe el objeto de la notificaci贸n en JSON
$body = @file_get_contents('php://input');
$event_json = json_decode($body);

$id=$event_json->data->object->payment_details->product_id;
$customer_name= $event_json->data->object->payment_details->customer_name;
$payer_email= $event_json->data->object->payment_details->customer_email;
$customer_email= $event_json->data->object->payment_details->customer_email;
$succes=$event_json->type;

if($succes=='charge.success') {

    update_post_meta($id, 'payer_name', $customer_name);
    update_post_meta($id, 'payer_email', $payer_email);
    update_post_meta($id, 'payer_phone', $customer_phone);
    update_post_meta($id, 'estatus', 'En proceso');
    notifi_pagos($id);
}

?>