<?php
/* Template Name: contacto */

get_header();
$imgdestacada=wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<section style="background-image: url('<?php echo $imgdestacada; ?>')" class="contacto">
<div class="overlay_contacto"></div>
    <div class="row">
        <div class="center">
            <h3 class="titulo_contacto">Contacto</h3>
            <div class="divisor"></div>
            <div class="clearfix"></div>

            <div class="contenido_page">
                <div class="small-12 medium-6 large-6 columns info_contacto">
                    <?php
                    while ( have_posts() ) : the_post();
                        the_content();
                    endwhile;
                    ?>
                </div>
                <div class="small-12 medium-6 large-6 columns form_contacto">
                    <?php echo do_shortcode('[contact-form-7 id="6" title="Formulario de contacto 1"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>