<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eternitta
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header id="masthead" class="site-header" role="banner">
        <div class="menu_mobil"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/navigation.png"></a></div>
		<nav id="site-navigation" class="main-navigation" role="navigation">
            <div class="glyph-icon flaticon-delete30 close_menu"></div>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav>

        <div class="btn_distribuidores">Distribuidores</div>
        <div class="bienvenido"><a href="<?php echo site_url(); ?>/panel-distribuidor"><p></p></a><span class="close_sesion"> Cerrar</span></div>
        <div class="icon_cart"><span class="contador_pedidos">0</span><div class="glyph-icon flaticon-cart3"></div></div>
        <div class="list_pedidos">
            <ul>
            </ul>
            <div class="text-center"><a class="ver_carrito" href="<?php echo site_url(); ?>/pedido">Ver carrito</a></div>
        </div>
         <div class="current_moneda"></div>
</header>
<?php  if(function_exists("transposh_widget")) { transposh_widget(array(), array('title' => '', 'widget_file' => 'flags/tpw_flags_css.php')); }?>


